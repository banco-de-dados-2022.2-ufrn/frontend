import { Stack } from '@chakra-ui/react';
import {
  RiDashboardLine,
  RiContactsLine,
  RiMedicineBottleLine,
  RiSurveyLine,
} from 'react-icons/ri';
import NavLink from './NavLink';
import NavSection from './NavSection';

const SidebarNav = () => {
  return (
    <Stack spacing='12' align='flex-start'>
      <NavSection title='GERAL'>
        <NavLink icon={RiDashboardLine} href='/dashboard'>
          Dashboard
        </NavLink>
        <NavLink icon={RiContactsLine} href='/pacients'>
          Pacientes
        </NavLink>
        <NavLink icon={RiContactsLine} href='/nurses'>
          Enfermeiros
        </NavLink>
        <NavLink icon={RiContactsLine} href='/doctors'>
          Médicos
        </NavLink>
      </NavSection>

      <NavSection title='AUTOMAÇÃO'>
        <NavLink icon={RiMedicineBottleLine} href='/medicines'>
          Remédios
        </NavLink>
        <NavLink icon={RiSurveyLine} href='/exams'>
          Exames
        </NavLink>
      </NavSection>
    </Stack>
  );
};
export default SidebarNav;
