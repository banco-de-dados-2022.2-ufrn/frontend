import { Icon, Link as ChakraLink, LinkProps, Text } from '@chakra-ui/react';

import ActiveLink from '../ActiveLink';

interface NavLinkProps extends LinkProps {
  icon: React.ElementType;
  children: React.ReactNode;
  href: string;
}

const NavLink: React.FC<NavLinkProps> = ({ children, icon, href, ...rest }) => {
  return (
    <ActiveLink href={href} passHref>
      <ChakraLink display='flex' alignItems='center' {...rest}>
        <Icon as={icon} fontSize='20' />
        <Text ml='4' fontWeight='medium'>
          {children}
        </Text>
      </ChakraLink>
    </ActiveLink>
  );
};
export default NavLink;
