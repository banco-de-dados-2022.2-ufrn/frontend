import {
  FormControl,
  FormLabel,
  Input as InputChakara,
  InputProps as InputChakaraProps,
} from '@chakra-ui/react';

interface InputProps extends InputChakaraProps {
  name: string;
  label?: string;
}

export const Input: React.FC<InputProps> = ({ name, label, ...rest }) => {
  return (
    <FormControl>
      <FormLabel htmlFor={name}>{label}</FormLabel>
      <InputChakara
        id={name}
        name={name}
        focusBorderColor='pink.500'
        bgColor='gray.900'
        variant='filled'
        _hover={{
          bgColor: 'gray.900',
        }}
        size='lg'
        {...rest}
      />
    </FormControl>
  );
};
