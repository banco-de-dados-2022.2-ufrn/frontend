import { Box, Flex } from '@chakra-ui/react';
import Header from '../Header';

import { Sidebar } from '../Sidebar';

interface LayoutProps {
  children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <Box>
      <Header />

      <Flex w='100%' my='6' maxWidth={1480} mx='auto' px='6'>
        <Sidebar />

        <Box flex='1' borderRadius={8} bg='gray.800' p={['6', '8']}>
          {children}
        </Box>
      </Flex>
    </Box>
  );
};
export default Layout;
