import { Flex, Icon, IconButton, useBreakpointValue } from '@chakra-ui/react';
import { RiMenuLine } from 'react-icons/ri';
import { useSidebarDrawer } from '../../contexts/SidebarDrawerContext';
import Logo from './Logo';
import Notifications from './Notifications';
import Profile from './Profile';
import SearchBox from './SearchBox';

const Header = () => {
  const { onOpen } = useSidebarDrawer();

  const isWideVersion = useBreakpointValue({
    base: false,
    lg: true,
  });

  return (
    <Flex
      as='header'
      w='100%'
      maxWidth={1480}
      h='20'
      mx='auto'
      mt='4'
      px='6'
      align='center'
    >
      {isWideVersion ? null : (
        <IconButton
          icon={<Icon as={RiMenuLine} />}
          fontSize='24'
          onClick={onOpen}
          aria-label='Open navigation'
          mr='2'
          variant='unstyled'
          _hover={{ bg: 'gray.900' }}
          _active={{ bg: 'gray.900' }}
        />
      )}

      <Logo />

      {isWideVersion ? <SearchBox /> : null}

      <Flex align='center' ml='auto'>
        <Notifications />

        <Profile showProfileData={isWideVersion} />
      </Flex>
    </Flex>
  );
};
export default Header;
