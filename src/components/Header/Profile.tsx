import { Avatar, Box, Flex, Text } from '@chakra-ui/react';

interface ProfileProps {
  showProfileData?: boolean;
}

const Profile: React.FC<ProfileProps> = ({ showProfileData = true }) => {
  return (
    <Flex align='center'>
      {showProfileData ? (
        <Box mr='4' textAlign='right'>
          <Text>Matthieu Christian</Text>
          <Text color='gray.300' fontSize='small'>
            matthieuceo@gmail.com
          </Text>
        </Box>
      ) : null}
      <Avatar
        size='md'
        name='Matthieu Christian'
        src='https://github.com/mateusdrag1.png'
      />
    </Flex>
  );
};
export default Profile;
