import { theme } from '../styles/theme';

export const options = {
  chart: {
    toolbar: {
      show: false,
    },

    zoom: {
      enabled: false,
    },

    foreColor: theme.colors.gray[500],
  },
  grid: {
    show: false,
  },
  dataLabels: {
    enabled: false,
  },
  tooltip: {
    enabled: false,
  },
  xaxis: {
    type: 'datetime' as const,
    axisBorder: {
      color: theme.colors.gray[600],
    },
    axisTicks: {
      color: theme.colors.gray[600],
    },
    categories: [
      '2022-12-18T00:00:00.000Z',
      '2022-12-19T00:00:00.000Z',
      '2022-12-20T00:00:00.000Z',
      '2022-12-21T00:00:00.000Z',
      '2022-12-22T00:00:00.000Z',
      '2022-12-23T00:00:00.000Z',
      '2022-12-24T00:00:00.000Z',
    ],
  },
  fill: {
    opacity: 0.3,
    type: 'gradient',
    gradient: {
      shade: 'dark',
      opacityFrom: 0.7,
      opacityTo: 0.3,
    },
  },
};

export const series: ApexAxisChartSeries = [
  { name: 'consultas', data: [31, 40, 28, 51, 42, 109, 100] },
];
