import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import Link from 'next/link';
import { Input } from '../../components/Form/Input';
import Layout from '../../components/Layout/Layout';

const CreatePacients = () => {
  return (
    <Layout>
      <Heading size='lg' fontWeight='normal'>
        Cadastrar Paciente
      </Heading>
      <Divider my='6' borderColor='gray.700' />

      <VStack spacing='8'>
        <SimpleGrid minChildWidth='240px' spacing={['6', '8']} w='100%'>
          <Input name='name' label='Nome completo' />
          <Input name='cpf' label='CPF' />
        </SimpleGrid>
      </VStack>

      <Flex mt='8' justify='flex-end'>
        <HStack spacing='4'>
          <Link href={'/pacients'} passHref>
            <Button colorScheme='whiteAlpha'>Cancelar</Button>
          </Link>
          <Button colorScheme='pink'>Salvar</Button>
        </HStack>
      </Flex>
    </Layout>
  );
};
export default CreatePacients;
