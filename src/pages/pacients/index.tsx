import {
  Box,
  Button,
  Checkbox,
  Flex,
  Heading,
  Icon,
  IconButton,
  Stack,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  useBreakpointValue,
} from '@chakra-ui/react';
import Link from 'next/link';
import { RiAddLine, RiPencilLine } from 'react-icons/ri';
import Header from '../../components/Header';

import Pagination from '../../components/Pagination';
import { Sidebar } from '../../components/Sidebar';

function PacientList() {
  const isWideVersion = useBreakpointValue({
    base: false,
    lg: true,
  });

  return (
    <Box>
      <Header />

      <Flex w='100%' my='6' maxWidth={1480} mx='auto' px='6'>
        <Sidebar />

        <Box flex='1' borderRadius={8} bg='gray.800' p='8'>
          <Flex mb='8' justify='space-between' alignItems='center'>
            <Heading size='lg' fontWeight='normal'>
              Pacientes
            </Heading>

            <Link href={'/pacients/create'} passHref>
              <Button
                size='sm'
                fontSize='sm'
                colorScheme='pink'
                leftIcon={<Icon as={RiAddLine} />}
                as='a'
              >
                Novo Paciente
              </Button>
            </Link>
          </Flex>

          <Table colorScheme='whiteAlpha'>
            <Thead>
              <Tr>
                <Th px={['4', '4', '6']} color='gray.300' width='8'>
                  <Checkbox colorScheme='pink' />
                </Th>
                <Th>Nome</Th>
                {isWideVersion ? <Th>CPF</Th> : null}

                <Th width='8'>Opções</Th>
              </Tr>
            </Thead>
            <Tbody>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(item => (
                <Tr key={item}>
                  <Td px={['4', '4', '6']}>
                    <Checkbox colorScheme='pink' />
                  </Td>
                  <Td>João da Silva</Td>
                  {isWideVersion ? <Td>000.000.000-00</Td> : null}

                  <Td>
                    <Stack direction='row' spacing='4'>
                      {isWideVersion ? (
                        <Button
                          size='sm'
                          fontSize='sm'
                          colorScheme='purple'
                          leftIcon={<Icon as={RiPencilLine} fontSize='16' />}
                        >
                          Editar
                        </Button>
                      ) : (
                        <IconButton
                          icon={<RiPencilLine />}
                          fontSize='16'
                          colorScheme='purple'
                          aria-label='Editar'
                        />
                      )}
                    </Stack>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
          <Pagination />
        </Box>
      </Flex>
    </Box>
  );
}
export default PacientList;
