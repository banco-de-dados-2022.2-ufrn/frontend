import { Box, Flex, SimpleGrid, Text } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import Header from '../components/Header';

import { Sidebar } from '../components/Sidebar';
import { options, series } from '../constants';

const Chart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export default function Dashboard() {
  return (
    <Flex direction='column' h='100vh'>
      <Header />

      <Flex w='100%' my='6' maxWidth={1480} mx='auto' px='6'>
        <Sidebar />

        <SimpleGrid
          flex='1'
          gap='4'
          minChildWidth='320px'
          alignItems='flex-start'
        >
          <Box bg='gray.800' p={['6', '8']} borderRadius={8}>
            <Text fontSize='lg' mb='4'>
              Consultas semanais:
            </Text>
            <Chart type='area' height={160} options={options} series={series} />
          </Box>
          <Box bg='gray.800' p={['6', '8']} borderRadius={8}>
            <Text fontSize='lg' mb='4'>
              Consultas mensais:
            </Text>
            <Chart type='area' height={160} options={options} series={series} />
          </Box>
        </SimpleGrid>
      </Flex>
    </Flex>
  );
}
