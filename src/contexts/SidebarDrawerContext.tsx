import { useDisclosure } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { createContext, useContext, useEffect } from 'react';

interface SidebarDrawerContextData extends ReturnType<typeof useDisclosure> {}

interface SidebarDrawerProviderProps {
  children: React.ReactNode;
}

const SidebarDrawerContext = createContext({} as SidebarDrawerContextData);

export const SidebarDrawerProvider: React.FC<SidebarDrawerProviderProps> = ({
  children,
}) => {
  const store = useDisclosure();
  const router = useRouter();

  useEffect(() => {
    store.onClose();
  }, [router.asPath, store]);

  return (
    <SidebarDrawerContext.Provider value={store}>
      {children}
    </SidebarDrawerContext.Provider>
  );
};

export const useSidebarDrawer = () => useContext(SidebarDrawerContext);
